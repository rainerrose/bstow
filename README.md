# bstow

Implementation of the fantastical software `stow` in bash.

I needed it once when I needed to compile lua but didn't want to install all the perl dependencies.

## Installation

Put the script somewhere where `$PATH` will find it.

## Usage

Put the script in the place of the installed software, for example

~~~ bash
cd /opt/
bstowh.sh lua-5.4.4
~~~

Then all subdirectories under `lua-5.4.4` will be symlinked to `/usr/local`.

Example of directorystructure
~~~
.
|-- bin
|   |-- lua
|   `-- luac
|-- include
|   |-- lauxlib.h
|   |-- lua.h
|   |-- lua.hpp
|   |-- luaconf.h
|   `-- lualib.h
|-- lib
|   |-- liblua.a
|   `-- lua
|       `-- 5.4
|-- man
|   `-- man1
|       |-- lua.1
|       `-- luac.1
`-- share
    `-- lua
        `-- 5.4

10 directories, 10 files
~~~

To undo the symlinks use the `-D` parameter. All **empty** subdirectories from the second level on will be deleted.

~~~ bash
cd /opt/
bstowh.sh lua-5.4.4 -D
~~~
