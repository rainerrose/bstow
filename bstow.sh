#!/bin/bash
DIRECTORY="${1}"
DELETE_YN="${2}"

function usage ()
{
    echo "$0 <DIRECTORY|-h> [<-D>]" >&2
    echo " " >&2
    echo "Parameter:" >&2
    echo " " >&2
    echo "-h help" >&2
    echo "-D delete sym-links in /usr/local " >&2
    echo " " >&2
    exit 1
}

test -z "${DIRECTORY}" && usage
test "${DIRECTORY}" = "-h" && usage
test -d "${DIRECTORY}" || usage

cd "${DIRECTORY}"
if [ "${DELETE_YN}" = "-D" ]
then
    # Erst die Dateien
    find . -type f -print | while read N
    do 
        echo "Datei ${N}"
        rm "/usr/local/${N}" 
    done

    # dann die Verzeichnisse, aber erst zweite Ebene /usr/local/bin, z.B. stehen lassen
    find . -mindepth 2 -type d -print | sort -r | while read N
    do 
        echo "Verz. /usr/local/$N"
        rmdir --ignore-fail-on-non-empty "/usr/local/${N}"
    done
else
    # Erst die Verzeichnisse
    find . -type d -print | while read N
    do
        echo "Verz. $N"
        test -e "/usr/local/${N}" || mkdir -p  "/usr/local/${N}"
    done
    
    # dann die Dateien
    find . -type f -print | while read N
    do
        echo "Datei $N /usr/local/${N}"
        ln -s -f "$(pwd)/${N}" "/usr/local/${N}"
    done
fi 
cd ..
